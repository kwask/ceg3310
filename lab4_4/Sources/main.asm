;**************************************************************
;* This program serves as a sample code to test the           *
;* "Terminal" component when using a "Full Chip simulation".  *
;* When you execute this program, a "Hello! Please enter ..." *
;* message shows up in the terminal window.                   *
;*                                                            *
;* To open a Terminal window, use the menu "Component",       *
;* sub-menu "Open" within the debug window environment, and   *
;* click on the Terminal icon.                                *
;* Press the right mouse button and select the "Configure     *
;* Connection" option.  Then select the "Virtual SCI" as the  *
;* default configuration.  As to the virtual SCI input/output *
;* ports, replace Sci0 with Sci. For example,                 *
;* Sci0.SerialOutput becomes Sci.SerialOutput                 *
;**************************************************************
; Include derivative-specific definitions
            INCLUDE 'derivative.inc'

; export symbols
            XDEF Entry, _Startup, main
            ; we use export 'Entry' as symbol. This allows us to
            ; reference 'Entry' either in the linker .prm file
            ; or from C/C++ later on

            XREF __SEG_END_SSTACK ; symbol defined by the linker for the end of the stack


; Constant Data Section
DATA: SECTION
Hello_msg   dc.b "Enter a number:"
Newline     dc.b $0a
Endline     dc.b $0D
EndofHello  dc.b 0
EndofRecv   dc.b 0

; Variable Data Section
MY_EXTENDED_RAM: SECTION
val         ds.b  1
n           ds.b  1
n_counter   ds.b  1
fn          ds.w  1


; Code Section
MyCode:     SECTION
main:
_Startup:
Entry:
; Set up the SP for subroutine calls
          LDS   #__SEG_END_SSTACK
          
; Initialize SCI control registers
          JSR   SCI_init
          
; Print out the "Hello ..." message
          LDX   #Hello_msg      
          BSR   writeStr
          
          MOVB  #0,SP
          DES
          
; Receive the string
loop1:    BRCLR SCISR1, #$20, loop1       
          LDAB  SCIDRL

; checking if char was endline
          CMPB  Endline
          BEQ   EndLoop1 

; outputting the character
          STAB  SCIDRL
loop2:    BRCLR SCISR1, #$80, loop2

; storing the character in the stack pointer
          STAB  SP
          DES
          BRA loop1
           
EndLoop1: INS

; calculating n
          LDAA  #0   
          LDAB  SP
          SUBB  #'0'
          STAB  n
          INS
          LDAB  SP
          CPD   #0
          BEQ   ConFin
          
          LDAA  #0
          LDAB  SP
          SUBB  #'0'
          LDY   #10
          EMUL
          ADDB  n
          STAB  n
          INS
          LDAB  SP
          CPD   #0
          BEQ   ConFin
          
          LDAA  #0   
          LDAB  SP
          SUBB  #'0'
          LDY   #100
          EMUL
          ADDB  n
          STAB  n
          INS
ConFin:                    
     
; calculating F(n)
          LDY   #n
          JSR   FSeq
          
          BRA   loop1  ; Repeat the process
          
;****** END OF MAIN ************************************          

; A subroutine to print out a string
; Register X stores the starting address of the string
writeStr: LDAB  0,x
          CMPB  #$0
          BEQ   complete
loopStr2:  BRCLR SCISR1, #$80, loopStr2
          STAB  SCIDRL
          INX  
          BRA   writeStr
complete: RTS

; A subroutine to initialize the SCI port
; No need for any argument
SCI_init: LDAA  #$4c
          STAA  SCICR1
          LDAA  #$0c
          STAA  SCICR2
          LDAA  #52
          STAA  SCIBDL
          RTS

; Subroutine to calculate the sequence from lab 3
FSeq:       PSHD
            PSHY
            BRCLR SP,#01,Return          ; jump to the infinite end loop if n is even
            
            ; sets up our output and temporary variables
            MOVW  $0,fn                 ; clears value  
            LDX   SP
            LDAA  X              
            STAA  n_counter                ; starts n_counter at n
            
            ; calculates value
Exponent:   CLRA
            LDAB  n_counter                  
            XGDY
            CLRA
            LDAB  n_counter
            EMUL                        ; squares the factor
             
            ADDD  fn                    ; adding value to newly calculated exponent
            STD   fn                    ; storing new value
            
            DEC   n_counter 
            TST   n_counter
            BEQ   Return                ; branch if n_counter is 0
            
            DEC   n_counter       
            TST   n_counter
            BEQ   Return                ; branch if n_counter is 0
            BRA   Exponent

Return:     PULY
            PULD
            RTS