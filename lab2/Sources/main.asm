;**************************************************************
;*          Lab 2                         Summer, 2019        *
;*                                                            *
;**************************************************************
; Include derivative-specific definitions
            INCLUDE 'derivative.inc'

; export symbols
            XDEF Entry, _Startup, main
            ; we use export 'Entry' as symbol. This allows us to
            ; reference 'Entry' either in the linker .prm file
            ; or from C/C++ later on

            XREF __SEG_END_SSTACK      ; symbol defined by the linker for the end of the stack

            ; defining our constants and variables
Constants:  SECTION
input_val:  DC.B  $0F                   ; input value
char_offset:DC.B  '0'                   ; offset value for character conversion 
char_pos:   DC.B  '+'
char_neg:   DC.B  '-'

Variables:  SECTION.                 
str:        DS.B  4                     ; output string

            ; program logic begins here
Code:       SECTION
main:
_Startup:
Entry:
            LDS   #__SEG_END_SSTACK     ; initialize the stack pointer
            CLI                         ; enable interrupts
          
            LDAB   input_val            ; load our value into D
            MOVB  #$FF,DDRP             ; sets all bits on port P to output
            MOVB  #$FF,PTP              ; sets all bits on port P to active
            BRCLR input_val,#$80,PostLight
            
            MOVB  char_neg,str          ; copies the minus character to str
            COMB                        ; one's complement on B
            ADDD  #$1                   ; completes two's complement on B
            MOVB  #$F7,PTP              ; turns on red light
            BRA Calcu                   ; jump to the character conversion
PostLight:
            MOVB  #$EF,PTP              ; turns on the green light
            MOVB  char_pos,str          ; copies the plus character to str
Calcu:
            ; generating the character in the 100s place
            LDX   #100                  ; load our divisor for the 100s place into X
            IDIV                        ; divide D by X, quotient is in X, remainder in D
            
            XGDX                        ; swap quotient into D
            ADDB  char_offset           ; add our character offset to B
            STAB  str+1                   ; store B at str
            XGDX                        ; swap remainder into D
            
            ; generating the character in the 10s place
            LDX   #10                   ; load our divisor for the 10s place into X
            IDIV                        ; divide D by X
            
            XGDX                        ; swap quotient into D
            ADDB  char_offset           ; add our character offset to B
            STAB  str+2                 ; store B at str+1
            XGDX                        ; swap remainder into D
            
            ; generating the character in the 1s place
            ADDB  char_offset           ; add our character offset to B
            STAB  str+3                 ; store B at str+2 
            
self:       BRA   self                  ; program loops here forever 
