;**************************************************************
;* This program serves as a sample code to test the           *
;* "Terminal" component when using a "Full Chip simulation".  *
;* When you execute this program, a "Hello! Please enter ..." *
;* message shows up in the terminal window.                   *
;*                                                            *
;* To open a Terminal window, use the menu "Component",       *
;* sub-menu "Open" within the debug window environment, and   *
;* click on the Terminal icon.                                *
;* Press the right mouse button and select the "Configure     *
;* Connection" option.  Then select the "Virtual SCI" as the  *
;* default configuration.  As to the virtual SCI input/output *
;* ports, replace Sci0 with Sci. For example,                 *
;* Sci0.SerialOutput becomes Sci.SerialOutput                 *
;**************************************************************
; Include derivative-specific definitions
            INCLUDE 'derivative.inc'

; export symbols
            XDEF Entry, _Startup, main
            ; we use export 'Entry' as symbol. This allows us to
            ; reference 'Entry' either in the linker .prm file
            ; or from C/C++ later on

            XREF __SEG_END_SSTACK ; symbol defined by the linker for the end of the stack


; Constant Data Section
DATA: SECTION
Hello_msg   dc.b "Enter a number:"
Newline     dc.b $0a
Endline     dc.b $0D
EndofHello  dc.b 0
EndofRecv   dc.b 0
log         dc.b 10

; Variable Data Section
MY_EXTENDED_RAM: SECTION
n           ds.b  1
mult        ds.w  1
fn          ds.w  1


; Code Section
MyCode:     SECTION
main:
_Startup:
Entry:
; Set up the SP for subroutine calls
            LDS   #__SEG_END_SSTACK
          
; Initialize SCI control registers
            JSR   SCI_init
          
; Clears global vars
            CLR   n
            CLR   fn
          
; read input          
            JSR   ReadN         
 ; calculating F(n)
            CLRA
            LDAB  n
            JSR   Rfn
            
            MOVW  -4,SP,fn
            
Self:       BRA   Self ; infintie loop
          
;****** END OF MAIN ************************************          

; A subroutine to print out a string
; Register X stores the starting address of the string
writeStr:   LDAB  0,x
            CMPB  #$0
            BEQ   complete
loopStr2:   BRCLR SCISR1, #$80, loopStr2
            STAB  SCIDRL
            INX  
            BRA   writeStr
complete:   RTS

; A subroutine to initialize the SCI port
; No need for any argument
SCI_init:   LDAA  #$4c
            STAA  SCICR1
            LDAA  #$0c
            STAA  SCICR2
            LDAA  #52
            STAA  SCIBDL
            RTS


; A subroutine to read user input and convert it into a two-byte int
ReadN:      LDX   #Hello_msg      
            BSR   writeStr
            
            PSHD
            PSHY
            DES
            MOVB  #0,SP

; reading in the next user input            
Reading:    DES
InLoop:     BRCLR SCISR1, #$20, InLoop
            LDAB  SCIDRL

; checking if char was endline
            CMPB  Endline
            BEQ   EndLoop1 

; outputting the character
            STAB  SCIDRL
OutLoop:    BRCLR SCISR1, #$80, OutLoop

; storing the character in the stack pointer
            STAB  SP
            BRA   Reading
           
EndLoop1:   INS

; converting characters into int
            MOVW  #1,mult
            CLR   n
            
ConvLoop:   CLRA   
            LDAB  SP
            CPD   #0
            BEQ   ConFin
            SUBB  #'0'
            LDY   mult
            EMUL
            
            ; adding value to existing sum
            ADDB  n
            STAB  n
            INS
            
            ; increasing our multiplier
            LDD   mult
            LDAA  log
            MUL
            STD   mult
            BRA   ConvLoop
            
ConFin:     INS
            PULY
            PULD
            RTS


;A subroutine to calculate the sequence from lab 3
Rfn:        LEAS -2,SP
            MOVW  $0,0,SP               ; clears return value
            PSHD
            PSHY
            
            BRCLR 3,SP,#01,Return       ; jump to the infinite end loop if n is even
            
            ; calculates value
            LDD   2,SP                  ; putting n into D                   
            XGDY                        ; swapping n into X                       
            LDD   2,SP
            EMUL                        ; squares the factor
            
            ; store result
            STD   4,SP
            
            ; decrementing n by 2, return early if this is the last call
            LDD   2,SP
            DECB
            TSTB
            BEQ   Return
            
            DECB
            TSTB
            BEQ   Return
            
            ; calling recursively
            JSR Rfn
            
            ; adding recursive return result to this return result
            LDD   -4,SP
            ADDD  4,SP
            STD   4,SP

Return:     PULY
            PULD
            LEAS 2,SP
            RTS