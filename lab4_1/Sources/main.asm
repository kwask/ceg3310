;**************************************************************
;* This stationery serves as the framework for a              *
;* user application. For a more comprehensive program that    *
;* demonstrates the more advanced functionality of this       *
;* processor, please see the demonstration applications       *
;* located in the examples subdirectory of the                *
;* Freescale CodeWarrior for the HC12 Program directory       *
;**************************************************************
; Include derivative-specific definitions
            INCLUDE 'derivative.inc'

; export symbols
            XDEF Entry, _Startup, main
            ; we use export 'Entry' as symbol. This allows us to
            ; reference 'Entry' either in the linker .prm file
            ; or from C/C++ later on

            XREF __SEG_END_SSTACK      ; symbol defined by the linker for the end of the stack




; variable/data section
GlobalConstants: SECTION
n:          DC.B   $5
cycles:     DC.W   $09E0
toggle:     DC.B   $08

GlobalVariables: SECTION
; byte space definitions
value:      DS.W   1                  ; output number
n_counter:  DS.B   1                  ; current number counter
exp:        DS.W   1                  ; calculated exponent value

; code section
MyCode:     SECTION
main:
_Startup:
Entry:
            LDS  #__SEG_END_SSTACK    ; initialize the stack pointer
            CLI                       ; enable interrupts
            MOVB  #00,DDRT
            MOVB  #00,PTT
            MOVB  #$FF,PTP            ; turning on red light
            MOVB  #$FF,DDRP           ; sets all bits on port P to output
            
            ; turns on red light and checks if even or odd
Start:      BRCLR PTT,#$40,Clear
            JSR Timer
Clear:      BRA Start
            
               
            ; toggles light every 2 seconds
Timer:      MOVB  #$F7,PTP            ; red light on
            LDX   cycles              ; resets X back to max number of cycles
XStart:     LDD   cycles              ; resets D back to max number of cycles
DStart:     NOP                       ; eats 1 CPU cycle
            DBNE  D,DStart            ; decrements D then jumps back to DStart if != 0
            DBNE  X,XStart            ; decrements X then jumps back to XStart if != 0
            
            MOVB  #$FF,PTP            ; TURNS off red light
            RTS 