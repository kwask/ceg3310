;**************************************************************
;* This stationery serves as the framework for a              *
;* user application. For a more comprehensive program that    *
;* demonstrates the more advanced functionality of this       *
;* processor, please see the demonstration applications       *
;* located in the examples subdirectory of the                *
;* Freescale CodeWarrior for the HC12 Program directory       *
;**************************************************************
; Include derivative-specific definitions
            INCLUDE 'derivative.inc'

; export symbols
            XDEF Entry, _Startup, main
            ; we use export 'Entry' as symbol. This allows us to
            ; reference 'Entry' either in the linker .prm file
            ; or from C/C++ later on

            XREF __SEG_END_SSTACK      ; symbol defined by the linker for the end of the stack




; variable/data section
GlobalConstants: SECTION
n:          DC.B   $5
cycles:     DC.W   $06FC
toggle:     DC.B   $08

GlobalVariables: SECTION
; byte space definitions
value:      DS.W   1                  ; output number
n_counter:  DS.B   1                  ; current number counter
exp:        DS.W   1                  ; calculated exponent value

; code section
MyCode:     SECTION
main:
_Startup:
Entry:
            LDS  #__SEG_END_SSTACK    ; initialize the stack pointer
            CLI                       ; enable interrupts

            ; turns on red light and checks if even or odd
            MOVB  #$F7,PTP            ; turning on red light
            MOVB  #$FF,DDRP           ; sets all bits on port P to output
            BRCLR n,#01,EvenLoop      ; jump to the infinite end loop if n is even
            
            ; sets up our output and temporary variables
            MOVW  $0,value            ; clears value                
            MOVB  n,n_counter         ; starts n_counter at n
            
            ; calculates value
Exponent:   CLR   Y
            LDAB  n_counter
            ABY
            LDD   $0                  ; clears D
Multiply:   ADDB  n_counter           ; adds current number to D
            ADCA  $0
            DBNE  Y,Multiply          ; adds current number to D n_counter of times               
            ADDD  value               ; adding newly calculate exponent to value
            STD   value               ; storing new value
            DEC   n_counter 
            TST   n_counter
            BEQ   Reset            ; branch if n_counter is still not 0
            DEC   n_counter       
            TST   n_counter
            BEQ   Reset            ; branch if n_counter is still not 0
            BRA   Exponent
            
            ; toggles light every 2 seconds
Reset:      LDX   cycles              ; resets X back to max number of cycles
XStart:     LDD   cycles              ; resets D back to max number of cycles
DStart:     NOP                       ; eats 1 CPU cycle
            DBNE  D,DStart            ; decrements D then jumps back to DStart if != 0
            DBNE  X,XStart            ; decrements X then jumps back to XStart if != 0
            
            LDAA  PTP                 ; gets current status of port P
            EORA  toggle              ; toggles red light
            STAA  PTP                 ; sets new status of port P
            BRA   Reset               ; jumps back to beginnning of loops
EvenLoop:   BRA   EvenLoop