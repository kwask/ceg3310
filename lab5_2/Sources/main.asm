;**************************************************************
;* This stationery serves as the framework for a              *
;* user application. For a more comprehensive program that    *
;* demonstrates the more advanced functionality of this       *
;* processor, please see the demonstration applications       *
;* located in the examples subdirectory of the                *
;* Freescale CodeWarrior for the HC12 Program directory       *
;**************************************************************

; export symbols
            XDEF SCI_init, ReadN, Rfn
            ; we use export 'Entry' as symbol. This allows us to
            ; reference 'Entry' either in the linker .prm file
            ; or from C/C++ later on

; Include derivative-specific definitions 
		INCLUDE 'derivative.inc'  		

DATA: SECTION
PROMPT_MSG  dc.b "Enter a number:"
LOG         dc.b 10
ENDLINE     dc.b $0D


; Variable Data Section
MY_EXTENDED_RAM: SECTION
		    
; code section
MyCode:     SECTION


; ******************* WriteStr subroutine **********************
; A subroutine to print out a string
; Register X stores the starting address of the string
WriteStr:   LDAB  0,x
            CMPB  #$0
            BEQ   complete
loopStr2:   BRCLR SCISR1, #$80, loopStr2
            STAB  SCIDRL
            INX  
            BRA   WriteStr
complete:   RTC


; ******************* SCI_init subroutine **********************
; A subroutine to initialize the SCI port
SCI_init:   LDAA  #$4c
            STAA  SCICR1
            LDAA  #$0c
            STAA  SCICR2
            LDAA  #52
            STAA  SCIBDL
            RTC


; ******************* ReadN subroutine **********************
; A subroutine to read user input and convert it into a one-byte unsigned char
ReadN:      LDX     #PROMPT_MSG     
            CALL    WriteStr
               
                
            ; no parameters passed in, clear and push D            
            LDD     #0
            PSHD                
            PSHY
            PSHX
            
            ; setting up local var for multiplier
            LEAS    -2,SP
            MOVW    #1,SP

            ; setting up terminating char
            LEAS    -1,SP
            MOVB    #0,SP
            
            ; transfer SP loc to X
            TSX           

; reading in the next user input            
Reading:    DEX
InLoop:     BRCLR   SCISR1, #$20, InLoop
            LDAB    SCIDRL

; checking if char was endline
            CMPB    ENDLINE
            BEQ     EndInput

; outputting the character that the user entered
            STAB    SCIDRL
OutLoop:    BRCLR   SCISR1, #$80, OutLoop

; storing the character in the stack pointer
            STAB    X
            BRA     Reading
           
EndInput:   INX

; converting characters into int
ConvLoop:   CLRA
            LDAB    X
            TSTB
            BEQ     ConFin
            SUBB    #'0'
            LDY     1,SP
            EMUL
            
            ; adding value to existing sum
            ADDB    8,SP
            STAB    8,SP
            INX
            
            ; increasing our multiplier
            LDD     1,SP
            LDAA    LOG
            MUL
            STD     1,SP
            BRA     ConvLoop
            
ConFin:     LEAS    3,SP
            PULX
            PULY
            PULD  ; return value will be in D
            
            RTC


; ******************* Rfn subroutine **********************
;A recursive subroutine to calculate the sum of the sequence from lab 3
Rfn:        PSHD
            PSHY
            LEAS    -2,SP
            
            BRCLR   5,SP,#01,Return       ; jump to return if n is even
            
            ; calculates value
            CLRA
            LDAB    5,SP                  ; putting n into D                   
            XGDY                          ; swapping n into Y                       
            CLRA
            LDAB    5,SP
            EMUL                          ; squares the factor
            
            ; store result in local var
            STD     0,SP
            
            ; decrementing n by 2, return early if this is the last call
            LDAB    5,SP
            DECB
            TSTB
            BEQ     Return
            
            DECB
            TSTB
            BEQ     Return
            
            ; calling recursively
            CALL    Rfn
            
            ; adding recursive return result to this return result
 Return:    ADDD    0,SP
            LEAS    2,SP
            PULY       
            LEAS    2,SP ; D has been consumed, no need to pull
            RTC
