#include <hidef.h>      /* common defines and macros */
#include "derivative.h"      /* derivative-specific definitions */

#include "main_asm.h" /* interface to the assembly module */

void SCI_init(void);
unsigned char ReadN(void);
unsigned int  Rfn(unsigned char n);

unsigned char n;
unsigned int fn;

void main(void) {
  SCI_init(); // initialize IO ports
  n = ReadN(); // get user input
  fn = Rfn(n); // calculate sequence sum

  for(;;) {
    _FEED_COP(); /* feeds the dog */
  } /* loop forever */
  /* please make sure that you never leave main */
}
