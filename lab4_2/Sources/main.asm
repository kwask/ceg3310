;**************************************************************
;* This program serves as a sample code to test the           *
;* "Terminal" component when using a "Full Chip simulation".  *
;* When you execute this program, a "Hello! Please enter ..." *
;* message shows up in the terminal window.                   *
;*                                                            *
;* To open a Terminal window, use the menu "Component",       *
;* sub-menu "Open" within the debug window environment, and   *
;* click on the Terminal icon.                                *
;* Press the right mouse button and select the "Configure     *
;* Connection" option.  Then select the "Virtual SCI" as the  *
;* default configuration.  As to the virtual SCI input/output *
;* ports, replace Sci0 with Sci. For example,                 *
;* Sci0.SerialOutput becomes Sci.SerialOutput                 *
;**************************************************************
; Include derivative-specific definitions
            INCLUDE 'derivative.inc'

; export symbols
            XDEF Entry, _Startup, main
            ; we use export 'Entry' as symbol. This allows us to
            ; reference 'Entry' either in the linker .prm file
            ; or from C/C++ later on

            XREF __SEG_END_SSTACK ; symbol defined by the linker for the end of the stack


; Constant Data Section
DATA: SECTION
Hello_msg   dc.b "Hello! Please input a character."
Newline     dc.b $0a
EndofHello  dc.b 0
Receive_msg dc.b "Received: "
EndofRecv   dc.b 0

; Variable Data Section
MY_EXTENDED_RAM: SECTION
val         ds.b  1


; Code Section
MyCode:     SECTION
main:
_Startup:
Entry:
; Set up the SP for subroutine calls
          LDS   #__SEG_END_SSTACK
          
; Initialize SCI control registers
          BSR   SCI_init
          
; Print out the "Hello ..." message
          LDX   #Hello_msg      
          BSR   writeStr
 
; Receive one character
loop1:    BRCLR SCISR1, #$20, loop1       
          LDAB  SCIDRL
          STAB  val 
         
; Print out the "Received: " message
          LDX   #Receive_msg
          BSR   writeStr

; Print out the received character
          LDAB  val 
          STAB  SCIDRL
          LDAB  #$0a   ; print out a "new line" character
loop2:    BRCLR SCISR1, #$80, loop2
          STAB  SCIDRL
          BRA   loop1  ; Repeat the process
;****** END OF MAIN ************************************          

; A subroutine to print out a string
; Register X stores the starting address of the string
writeStr: LDAB  0,x
          CMPB  #$0
          BEQ   complete
loop3:    BRCLR SCISR1, #$80, loop3        
          STAB  SCIDRL
          INX  
          BRA   writeStr
complete: RTS

; A subroutine to initialize the SCI port
; No need for any argument
SCI_init: LDAA  #$4c
          STAA  SCICR1
          LDAA  #$0c
          STAA  SCICR2
          LDAA  #52
          STAA  SCIBDL
          RTS
